/* eslint-disable lines-between-class-members */
import React, { CSSProperties } from 'react';
import * as _ from 'lodash';

import { CancelTokenSource } from 'axios';
import { EditorState } from 'draft-js';
import { ListGroup, Spinner } from 'react-bootstrap';
import track, { TrackingProp } from 'react-tracking';
import { v4 as uuidv4 } from 'uuid';

import Log from '../../util/Log';
import API, { getCancelTokenSource, isCanceled } from '../../util/API';
import { ConditionConfig, Task, SpacyDocument } from '../../root-types';

import './Typeahead.scss';

type TypeaheadProps = {
  editorState: EditorState;
  condconf: ConditionConfig;
  task: Task;
  username: string;
  appendToText: (markdownText: string, suggestionId: string) => any;
  resetFocus: () => void;
  tracking?: TrackingProp;
};
type TypeaheadState = {
  typeahead: null | {
    suggestionId?: string;
    top: number;
    left: number;
    loading?: boolean;
    hasFocus?: boolean;
    suggestions?: SpacyDocument[];
    activeSuggestionIndex?: number;
    timeLoaded?: number;
    timeShown?: number;
  };
};

const TYPEAHEAD_TIMEOUT = 250;

@track(({ condconf }: TypeaheadProps) => ({ page: 'Typeahead', condconf }))
class Typeahead extends React.Component<TypeaheadProps, TypeaheadState> {
  activeSuggestionRef: React.RefObject<any>;
  typingTimeout?: NodeJS.Timeout;
  cancelTokenSource?: CancelTokenSource;

  constructor(props: TypeaheadProps) {
    super(props);
    this.state = {
      typeahead: null,
    };
    this.activeSuggestionRef = React.createRef();
  }

  componentDidMount() {
    this.updateTypeaheadPositionFromEvent();
    window.addEventListener('resize', this.updateTypeaheadPositionFromEvent);
    window.addEventListener('scroll', this.updateTypeaheadPositionFromEvent);
    window.addEventListener('keydown', this.globalKeyEvent);
  }

  componentDidUpdate(prevProps: TypeaheadProps, prevState: TypeaheadState) {
    const { editorState } = this.props;
    const { typeahead } = this.state;

    // editorState updated
    if (prevProps.editorState !== editorState) {
      Log.info('Editor state changed, recreate typehead.');

      // Workaround for nasty safari bug
      if (navigator.vendor.includes('Apple')) {
        setTimeout(() => this.removeTypeheadIfNotFocused(), 200);
      } else {
        window.requestAnimationFrame(() => this.removeTypeheadIfNotFocused());
      }

      // Reset timeout
      if (this.typingTimeout) clearTimeout(this.typingTimeout);
      const newTypingTimeout = setTimeout(this.triggerTypeahead, TYPEAHEAD_TIMEOUT);
      this.typingTimeout = newTypingTimeout;
    }

    // Active item changed, so we need to set focus
    if (prevState.typeahead?.activeSuggestionIndex !== typeahead?.activeSuggestionIndex) {
      this.activeSuggestionRef.current?.focus();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateTypeaheadPositionFromEvent);
    window.removeEventListener('scroll', this.updateTypeaheadPositionFromEvent);
    window.removeEventListener('keydown', this.globalKeyEvent);
  }

  getTypeaheadBoundingRect() {
    const { editorState } = this.props;
    if (
      !editorState.getSelection().getHasFocus() ||
      !editorState.isSelectionAtEndOfContent() ||
      !editorState.getSelection().isCollapsed() // Exclude selections of multiple characters
    )
      return undefined;

    const selection = window?.getSelection();
    const range = selection?.rangeCount && selection.getRangeAt(0).cloneRange();
    if (!range) return undefined;

    /* https://stackoverflow.com/a/59780954
       Even when using the default contenteditable of the browser there is indeed a weird behavior 
       when the cursor is set to a new line: the Range's getClientRects() will be empty and thus 
       getBoundingClientRect() will return a full 0 DOMRect.

       Therefore, in case we don't have client rects, but a startContainer, 
       we manually and explicitely select the contents of range.startContainer
     */
    if (!range.getClientRects().length && range.startContainer) {
      range.selectNodeContents(range.startContainer);
    }

    const rangeRect = range.getBoundingClientRect();
    range.detach();
    return [rangeRect.left, rangeRect.top];
  }

  globalKeyEvent = (event: KeyboardEvent) => {
    const { typeahead } = this.state;
    const { resetFocus } = this.props;

    // We don't care if we don't have suggestions
    if (!typeahead) return;

    if (this.removeTypeheadIfNotFocused()) return;

    const { suggestions, activeSuggestionIndex } = typeahead;

    const incIndex = (add: number) => {
      if (!suggestions?.length) return undefined;
      if (activeSuggestionIndex === undefined) return add > 0 ? 0 : suggestions.length - 1;
      return (activeSuggestionIndex + add + suggestions.length) % suggestions.length;
    };

    switch (event.keyCode) {
      case 40: // ArrowDown
        this.setState({ typeahead: { ...typeahead, activeSuggestionIndex: incIndex(1) } });
        event.preventDefault();
        break;

      case 38: // ArrowUp
        this.setState({ typeahead: { ...typeahead, activeSuggestionIndex: incIndex(-1) } });
        event.preventDefault();
        break;

      case 8: // Backspace
        this.resolveTypeahead({ resolution: 'dismissed-backspace' });
        break;

      case 46: // Delete
        this.resolveTypeahead({ resolution: 'dismissed-delete' });
        break;

      case 9: // Tab
        if (activeSuggestionIndex !== undefined) break;
        // eslint-disable-next-line no-case-declarations
        const firstSugg = (typeahead?.suggestions || [])[0];
        if (firstSugg) {
          event.preventDefault();
          this.append(firstSugg.text, typeahead?.suggestionId);
        }
        break;

      case 27: // Escape
        this.resolveTypeahead({ resolution: 'dismissed-escape' });
        event.preventDefault();
        resetFocus();
        if (this.typingTimeout) clearTimeout(this.typingTimeout);
        break;
    }
  };

  triggerTypeahead = async () => {
    const { editorState, task, username, tracking, condconf } = this.props;

    this.cancelTokenSource?.cancel();

    if (!this.getTypeaheadBoundingRect()) return;

    const text = `${task.subject}<|SEP|>${editorState.getCurrentContent().getPlainText().trim()}`;
    Log.info('Requesting typeahead for text: %s', text);
    if (!text) return;

    const replacements = _.clone(task.replacements);
    if (replacements['<PERSON>'] && _.includes(text, replacements['<PERSON>'])) {
      // Name was already used, now replace it with username
      replacements['<PERSON>'] = username;
    }

    const cancelTokenSource = getCancelTokenSource();
    this.cancelTokenSource = cancelTokenSource;

    const suggestionId = uuidv4();
    const timeLoaded = _.now();
    try {
      this.updateTypeaheadPosition({ suggestionId, loading: true, timeLoaded });
      tracking?.trackEvent({ action: 'suggestion-load', suggestionId, text });

      const {
        // eslint-disable-next-line @typescript-eslint/camelcase
        numberOfSuggestions: top_k,
        // eslint-disable-next-line @typescript-eslint/camelcase
        suggestionLength: max_length,
        // eslint-disable-next-line @typescript-eslint/camelcase
        stopAtSentenceEnd: stop_after_sentence,
      } = condconf;

      const res = await API.post<SpacyDocument[]>(
        '/generate-clause',
        {
          text,
          top_k,
          max_length,
          stop_after_sentence,
          replacements,
        },
        { cancelToken: cancelTokenSource.token }
      );

      const timeShown = _.now();
      this.updateTypeaheadPosition({ loading: false, suggestions: res.data, timeShown });
      tracking?.trackEvent({
        action: 'suggestion-show',
        suggestionId,
        loadTime: timeShown - timeLoaded,
        shownSuggestions: res.data.map((i) => i.text),
      });
    } catch (error) {
      if (isCanceled(error) && this.cancelTokenSource === cancelTokenSource) {
        tracking?.trackEvent({ action: 'suggestion-request-canceled', suggestionId });
        this.setState({ typeahead: null });
        return;
      }
      Log.error('Error fetching suggestions: %O', error);
    }
  };

  updateTypeaheadPositionFromEvent = (_event?: Event) => {
    this.updateTypeaheadPosition();
  };

  updateTypeaheadPosition(args?: Partial<TypeaheadState['typeahead']>) {
    this.setState((prevState) => {
      const rangeRect = this.getTypeaheadBoundingRect();
      if (!rangeRect) return { typeahead: null };

      const [left, top] = rangeRect;
      return { typeahead: { ...prevState.typeahead, left, top, ...args } };
    });
  }

  removeTypeheadIfNotFocused() {
    const { typeahead } = this.state;
    const { editorState } = this.props;
    if (typeahead?.hasFocus) return false;

    if (editorState.getSelection().getHasFocus() && editorState.isSelectionAtEndOfContent()) return false;

    this.resolveTypeahead({ resolution: 'dismissed-focus' });
    return true;
  }

  resolveTypeahead(logDetails: Record<string, string>) {
    const { typeahead } = this.state;
    const { tracking } = this.props;

    const { suggestionId, timeShown } = typeahead || {};
    const focusTime = timeShown ? _.now() - timeShown : 0;

    this.cancelTokenSource?.cancel();
    this.setState({ typeahead: null });

    tracking?.trackEvent({
      ...logDetails,
      action: 'suggestion-resolve',
      suggestionId,
      focusTime,
    });
  }

  append(text: string, suggestionId?: string) {
    const { appendToText } = this.props;
    Log.info('Appending text: %s', text);

    appendToText(text, suggestionId || 'NULL');
    this.resolveTypeahead({ resolution: 'selected', text });
    window.requestAnimationFrame(() => this.triggerTypeahead());
  }

  renderItem(item: SpacyDocument, index: number) {
    const { typeahead } = this.state;

    const tokens = item.tokens.map((token, idx, array) => {
      const text = item.text.substring(token.start, array[idx + 1]?.start || token.end);
      return { ...token, text };
    });
    const isActive = index === typeahead?.activeSuggestionIndex;

    if(!item.text.trim()) return null;

    return (
      <ListGroup.Item
        action
        onClick={() => this.append(item.text, typeahead?.suggestionId)}
        key={`${item.text}-${index}`}
        active={isActive}
        ref={isActive ? this.activeSuggestionRef : undefined}
      >
        {tokens.map((token) => {
          // // spaCy reference: https://spacy.io/api/annotation#pos-en
          // const tokenClassName = [`pos-${token.pos}`, `tag-${token.tag}`, `dep-${token.dep}`];

          return <span key={token.id}>{token.text}</span>;
        })}
      </ListGroup.Item>
    );
  }

  render() {
    const { typeahead } = this.state;
    if (!typeahead) return null;

    const { left, top, suggestions, loading } = typeahead!;
    const typeaheadStyle: CSSProperties = { position: 'fixed', left, top };
    return (
      <ListGroup
        className="typeahead-list shadow-lg bg-light rounded"
        style={typeaheadStyle}
        onFocus={() => this.setState({ typeahead: { ...typeahead, top, left, hasFocus: true } })}
        onBlur={() => this.setState({ typeahead: { ...typeahead, top, left, hasFocus: false } })}
      >
        {loading && (
          <ListGroup.Item disabled>
            <Spinner animation="border" size="sm" />
            &nbsp; Loading ...
          </ListGroup.Item>
        )}

        {!loading && suggestions && suggestions.map(this.renderItem.bind(this))}
      </ListGroup>
    );
  }
}

export default Typeahead;

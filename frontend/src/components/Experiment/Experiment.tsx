import React, { useEffect, useState } from 'react';
import { Card, Col, Row, Button, Alert } from 'react-bootstrap';
import * as _ from 'lodash';
import { useTracking } from 'react-tracking';

import InputArea from '../InputArea/InputArea';
import { TypefullyWindow, ConditionConfig, Task } from '../../root-types';

import useStateWithLocalStorage from '../../util/LocalStorage';

import './Experiment.scss';

declare let window: TypefullyWindow;

const KEEP_ALIVE_MS = 10000;

type ExperimentProps = {
  task: Task;
  initialCondconf: ConditionConfig;
  username: string;
  additionalInstructions?: string;
  onComplete: () => void;
};

// beforeunload event handler
const beforeUnloadHandler = (e: Event) => {
  e.preventDefault();
  e.returnValue = true;
};
const addUnloadWarning = () => window.addEventListener('beforeunload', beforeUnloadHandler, true);
const removeUnloadWarning = () => window.removeEventListener('beforeunload', beforeUnloadHandler, true);

const Experiment: React.SFC<ExperimentProps> = ({
  task,
  initialCondconf,
  username,
  additionalInstructions,
  onComplete,
}: ExperimentProps) => {
  const tracking = useTracking();

  const [condconf] = useStateWithLocalStorage<ConditionConfig>(`Task-${task.title}-ConditionConfig`, initialCondconf);
  const [text, setText] = useState('');

  // Track changes to condconf
  useEffect(() => {
    tracking.trackEvent({ action: 'set-condconf', condconf });
  }, [condconf]);

  // Track changes to text
  useEffect(() => {
    tracking.trackEvent({ action: 'changed-text', text });
  }, [text]);

  // Create Is-Alive log Interval
  useEffect(() => {
    const intVal = setInterval(() => tracking.trackEvent({ action: 'is-alive', text, condconf }), KEEP_ALIVE_MS);
    return () => {
      clearTimeout(intVal);
    };
  }, []);

  // track keyboard and mouse click event handler
  useEffect(() => {
    const onKeyDown = (event: KeyboardEvent) =>
      tracking.trackEvent({
        action: 'key-down',
        keyboardEvent: _.pick(event, ['key', 'altKey', 'ctrlKey', 'shiftKey', 'metaKey']),
      });

    window.addEventListener('keydown', onKeyDown);
    return () => {
      window.removeEventListener('keydown', onKeyDown);
    };
  }, []);

  useEffect(() => {
    addUnloadWarning();
    return removeUnloadWarning;
  }, []);

  const completeTask = async () => {
    removeUnloadWarning();
    tracking.trackEvent({ action: 'complete-task', text, condconf });
    onComplete();
  };

  const completeDisabled = text.split(' ').length <= 10;
  const hasSuggestions = !!condconf.numberOfSuggestions;

  return (
    <div className="experiment-component">
      <Row>
        <Col md={{ span: 8, offset: 2 }} className="mb-3">
          <Card>
            <Card.Header>Task</Card.Header>
            <Card.Body>
              {additionalInstructions && <Alert variant="info">{additionalInstructions}</Alert>}

              <Card.Text className="font-italic font-weight-bold border border-info rounded p-2">
                {task.description}
              </Card.Text>

              {hasSuggestions && (
                <Card.Text>
                  You are welcome to use the smart suggestions if you wish.
                </Card.Text>
              )}
              
              {!hasSuggestions && (
                <Card.Text>
                  For this task, please write the message without smart suggestions.
                </Card.Text>
              )}

              <Card.Text>
                As soon as you are finished with the task, please click on the blue button below the text field to
                continue.
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      </Row>

      <InputArea condconf={condconf} task={task} updateText={(val) => setText(val)} username={username} />

      <Row>
        <Col md={{ span: 8, offset: 2 }} className="mb-3">
          <Button block disabled={completeDisabled} onClick={() => completeTask()}>
            {completeDisabled ? 'Please write more to complete the task' : 'Complete task'}
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default Experiment;

/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';
import * as _ from 'lodash';
import { Modifier, SelectionState, EditorState, convertToRaw, Editor } from 'draft-js';
import { draftToMarkdown } from 'markdown-draft-js';

import Typeahead from '../Typeahead/Typeahead';
import { Task, ConditionConfig } from '../../root-types';

import './InputArea.scss';

type InputAreaProps = {
  condconf: ConditionConfig;
  task: Task;
  updateText: (val: string) => void;
  username: string;
};
type InputAreaState = {
  editorState: EditorState;
};

class InputArea extends React.Component<InputAreaProps, InputAreaState> {
  constructor(props: InputAreaProps) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty(),
    };
  }

  // eslint-disable-next-line react/destructuring-assignment
  getText(editorState = this.state.editorState) {
    return draftToMarkdown(convertToRaw(editorState.getCurrentContent()), {
      entityItems: {
        SUGGESTION: {
          open: (entity: any) =>
            [
              '<span',
              'class="suggestion"',
              `data-suggestion-id="${entity?.data?.suggestionId}"`,
              `data-suggestion-original-text="${entity?.data?.originalText}">`,
            ].join(' '),
          close: () => '</span>',
        },
      },
    });
  }

  // eslint-disable-next-line react/destructuring-assignment
  getPlainText(editorState = this.state.editorState) {
    return editorState.getCurrentContent().getPlainText();
  }

  // instance arrow functions go first
  // https://github.com/Microsoft/TypeScript/wiki/%27this%27-in-TypeScript#use-instance-functions
  appendToText = (text: string, suggestionId: string) => {
    let { editorState } = this.state;

    // Test if last character is whitespace - if so, remove it
    const lastBlock = editorState.getCurrentContent().getLastBlock();
    if (lastBlock.getText().slice(-1) === ' ') {
      // Remove last character. Apparently, there is _really_ no easier way.
      const selectionOffset = editorState.getSelection().getEndOffset();
      const key = lastBlock.getKey();
      const rmv = Modifier.removeRange(
        editorState.getCurrentContent(),
        new SelectionState({
          anchorKey: key,
          focusKey: key,
          anchorOffset: selectionOffset - 1,
          focusOffset: selectionOffset,
        }),
        'backward'
      );
      editorState = EditorState.push(editorState, rmv, 'delete-character');
    } else if (lastBlock.getText() === '') {
      // There is nothing we can trim in the text, then we need to trim the suggestion.
      // eslint-disable-next-line no-param-reassign
      text = text.trimLeft();
    }

    // Add entity
    let entityKey;
    if (suggestionId) {
      const contentStateWithEntity = editorState
        .getCurrentContent()
        .createEntity('SUGGESTION', 'MUTABLE', { suggestionId, originalText: text.trim() });
      entityKey = contentStateWithEntity.getLastCreatedEntityKey();
      editorState = EditorState.push(editorState, contentStateWithEntity, 'apply-entity');
    }

    const ncs = Modifier.insertText(
      editorState.getCurrentContent(),
      editorState.getSelection(),
      text,
      undefined,
      entityKey
    );
    editorState = EditorState.push(editorState, ncs, 'insert-fragment');

    // Move cursor
    editorState = EditorState.moveFocusToEnd(editorState);
    this.handleChange(editorState);
  };

  resetFocus = () => {
    let { editorState } = this.state;
    editorState = EditorState.moveFocusToEnd(editorState);
    this.setState({ editorState });
  };

  handleChange = (editorState: EditorState) => {
    const { updateText } = this.props;
    this.setState({ editorState });
    updateText(this.getText(editorState));
  };

  renderHeader(hasSuggestions: boolean): JSX.Element {
    const { username, task } = this.props;

    return (
      <Card className="border-bottom-0 email-header">
        <Card.Body className="p-3 d-flex">
          <div className="flex-grow-1 text-break">
            <Card.Title>New email</Card.Title>
            From: {`${_.kebabCase(username)}@online.com`}
            <br />
            To: {task.to}
            <br />
            Subject: {task.subject}
          </div>

          {hasSuggestions && (
            <div className="p-2 bg-light rounded" style={{ fontSize: '0.8rem', lineHeight: '1.5rem', minWidth: '25%' }}>
              Available Keyboard Shortcuts:
              <br />
              <kbd>Tab</kbd>
              : Confirm first suggestion
              <br />
              <kbd>&uarr;</kbd>
              <kbd>&darr;</kbd>: Select suggestion
              <br />
              <kbd>Enter</kbd>
              : Confirm suggestion
            </div>
          )}
        </Card.Body>
      </Card>
    );
  }

  render() {
    const { editorState } = this.state;
    const { username, condconf, task } = this.props;
    const hasSuggestions = !!condconf.numberOfSuggestions;

    return (
      <>
        <Row className="mb-4">
          <Col md={{ span: 8, offset: 2 }}>
            {this.renderHeader(hasSuggestions)}
            <div className="editor-wrapper p-3">
              <div className="text-append">{`Dear ${task.replacements['<|PERSON|>']},`}</div>
              <div className="editor">
                <Editor editorState={editorState} onChange={this.handleChange} />

                {hasSuggestions && (
                  <Typeahead
                    editorState={editorState}
                    username={username}
                    condconf={condconf}
                    task={task}
                    appendToText={this.appendToText}
                    resetFocus={this.resetFocus}
                  />
                )}
              </div>
              <div className="text-append">
                All the best,
                <br />
                {username}
              </div>
            </div>
          </Col>
        </Row>
      </>
    );
  }
}

export default InputArea;

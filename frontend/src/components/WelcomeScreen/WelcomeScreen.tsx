import React, { FormEvent, ChangeEvent } from 'react';
import { Button, Accordion, Card, Form, FormControl } from 'react-bootstrap';

type WelcomeScreenProps = {
  onComplete: (event?: any) => void;
  username: string;
  setUsername: (name: string) => void;
};

const WelcomeScreen: React.SFC<WelcomeScreenProps> = ({ onComplete, username, setUsername }: WelcomeScreenProps) => {
  return (
    <Accordion defaultActiveKey="0">
      <Card>
        <Card.Header>About</Card.Header>
        <Accordion.Collapse eventKey="0">
          <Card.Body>
            <Card.Text>
              In this study, you will test a new text editor, which supports composing an email with intelligent text
              suggestions.
              <br />
              The investigation of its use is the subject of the study, which is carried out in the context of a master
              thesis at LMU, Munich.
            </Card.Text>
            <Card.Text>
              The study consists of four tasks (3-5 minutes each) and five questionnaires. You will need about 30 minutes
              to complete the study.
            </Card.Text>
            If you wish to reach out to us, please use the following contact details:
            <ul>
              <li>Martin Zürn: martin.zuern@campus.lmu.de</li>
              <li>Malin Eiband: malin.eiband@ifi.lmu.de</li>
              <li>Daniel Buschek: daniel.buschek@uni-bayreuth.de</li>
            </ul>
            <Accordion.Toggle as={Button} eventKey="1">
              Continue
            </Accordion.Toggle>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Card.Header>Legal information</Card.Header>
        <Accordion.Collapse eventKey="1">
          <Card.Body>
            <Card.Title>Information about study participation and data processing</Card.Title>
            <Card.Text>
              1. I am aware that the collection, processing, and use of my data takes place voluntarily. Participation
              in the study can be canceled by me at any time without stating reasons, without this resulting in any
              disadvantages for me. In the event of termination, all data recorded by me up to that point will be
              irrevocably deleted after the data collection phase.
            </Card.Text>
            <Card.Text>
              1. I am aware that the collection, processing, and use of my data takes place voluntarily. Participation
              in the study can be canceled by me at any time without stating reasons, without this resulting in any
              disadvantages for me. In the event of termination, all data recorded by me up to that point will be
              irrevocably deleted after the data collection phase.
            </Card.Text>
            <Card.Text>
              2. I agree that my usage data when interacting with the website may be collected, stored, and processed in
              anonymized form. No personal data is processed.
            </Card.Text>
            <Card.Text>
              3. The results and original data of this study will be published as a scientific publication. This is done
              in a completely anonymous form, i.e. the data cannot be assigned to the respective participants in the
              study. The completely anonymized data of this might be made available as &quot;open data&quot; in a
              secure, internet-based repository. This study thus follows the recommendations of the German Research
              Foundation (DFG) for quality assurance in terms of verifiability and reproducibility of scientific
              results, as well as optimal post-use of data.
            </Card.Text>
            <Card.Text>
              Please click on the following document to read and download the
              {' '}
              <a href="/StatutoryDisclosure.pdf" target="_blank">
                Statutory Disclosure Agreement
              </a>
              .
            </Card.Text>
            <Card.Text>
              I hereby confirm that I have read and understood the above declaration of consent. I confirm that I
              voluntarily participate in the study and agree to the processing of my data by the
              Ludwig-Maximilians-Universität München, Germany.
            </Card.Text>
            <Accordion.Toggle as={Button} eventKey="2">
              I Agree
            </Accordion.Toggle>
            <Card.Text className="mt-2">
              <small>
                Unfortunately, you can only participate in the study if you agree to the aforementioned terms and
                conditions.
                <br />
                If you choose not to agree to the terms and conditions, please close this website.
              </small>
            </Card.Text>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card>
        <Card.Header>The task</Card.Header>
        <Accordion.Collapse eventKey="2">
          <Card.Body>
            <Card.Title>Now, here is your task.</Card.Title>
            <Card.Text>
              Please write an email according to the information that is presented in the next step.
            </Card.Text>
            <Card.Text>
              You can use the following keyboard shortcuts:
              <div className="p-2 bg-light rounded" style={{ fontSize: '0.8rem', lineHeight: '1.5rem', minWidth: '25%' }}>
                <kbd>Tab</kbd>
                : Confirm first suggestion
                <br />
                <kbd>&uarr;</kbd>
                <kbd>&darr;</kbd>
                : Select suggestion
                <br />
                <kbd>Enter</kbd>
                : Confirm selected suggestion
              </div>
            </Card.Text>
            <Form
              onSubmit={(e: FormEvent<HTMLFormElement>) => {
                e.preventDefault();
                onComplete();
              }}
            >
              <Form.Group>
                <Form.Label>Before we continue, please enter your name or any other name or nickname.</Form.Label>
                <Form.Control
                  required
                  value={username}
                  placeholder="John Doe"
                  onChange={(e: ChangeEvent<FormControl & HTMLInputElement>) => setUsername(e.currentTarget.value)}
                />
              </Form.Group>
              <Button type="submit">Ok, let&apos;s start.</Button>
            </Form>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  );
};

export default WelcomeScreen;

/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import * as _ from 'lodash';
import { Container, Navbar, Alert } from 'react-bootstrap';
import { v4 as uuidv4 } from 'uuid';

import useStateWithLocalStorage from '../../util/LocalStorage';
import Experiment from '../Experiment/Experiment';
import WelcomeScreen from '../WelcomeScreen/WelcomeScreen';
import SessionRecorder from '../SessionRecorder/SessionRecorder';
import API from '../../util/API';
import { latinSquare, Condition } from '../../taskDefinitions';
import LoggingRecorder from '../LoggingRecorder/LoggingRecorder';
import { ReactComponent as LogoIcon } from './logo-icon.svg';

const SURVEY_URL_AFTER_TASK = 'https://martinzuern.typeform.com/to/dUp3twNG';
const SURVEY_URL_FINAL = 'https://martinzuern.typeform.com/to/y1C5IONz';

const userIdParam = new URLSearchParams(window.location.search).get('user_id');
const userId = localStorage.getItem('UserId') || userIdParam || uuidv4();
localStorage.setItem('UserId', userId);

const prepareConditions = (conds: Condition[]): Required<Condition>[] => {
  return conds.map((el) => ({ ...el, session: uuidv4() }));
};

const isMobile = () => !_.isUndefined(window.orientation) || _.includes(navigator.userAgent, 'IEMobile');

const App: React.SFC<{}> = () => {
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);
  const [username, setUsername] = useStateWithLocalStorage<string>('Username', '');
  const [consent, setConsent] = useStateWithLocalStorage<boolean>('StudyConsent', false);
  const [lastCompletedTask, setLastCompletedTask] = useStateWithLocalStorage<number>('LastCompletedTask', -1);
  const [studyConditions, setStudyConditions] = useStateWithLocalStorage<Required<Condition>[]>('StudyConditions', []);

  // set conditions
  useEffect(() => {
    async function fetchData() {
      setLoading(true);
      if (!studyConditions.length) {
        await API.post('/log', [{ user: userId, action: 'init-application' }]);

        let lsRow: number = NaN;
        const lsRowParam = new URLSearchParams(window.location.search).get('ls_row');
        if (lsRowParam) {
          lsRow = Number.parseInt(lsRowParam, 10);
          console.log(`Received latin square row ${lsRow} via URL.`);
        }

        if (!(lsRow >= 0)) {
          lsRow = (await API.get<{ value: number }>('/participants')).data.value;
          console.log(`Received latin square row ${lsRow} via API.`);
        }

        lsRow %= latinSquare.length;
        console.log(`Using latin square row ${lsRow}.`);

        const conds = latinSquare[lsRow];
        setStudyConditions(prepareConditions(conds));
      }
      setLoading(false);
    }
    fetchData().catch((err) => {
      console.error(err);
      setError(true);
    });
  }, []);

  const redirectToSurvey = async (surveyUrl: string, params: { user_id: string } & Record<string, string | number>) => {
    setLoading(true);
    const targetURL = new URL(surveyUrl);
    Object.entries(params).forEach(([key, value]) => targetURL.searchParams.set(key, encodeURIComponent(value)));

    setTimeout(() => {
      window.location.href = targetURL.toString();
    }, 2500);
  };

  if (isMobile())
    return ErrorMessage(
      'Seems you are using a mobile device.',
      'This study can only be completed on a desktop computer. Please come back using the desktop browser of your choice.'
    );

  if (error) {
    return ErrorMessage(
      'Internal error.',
      'Please try again and contact us if error persists.'
    );
  }
  if (loading) {
    return <h1 className="m-5 text-center">Please wait ...</h1>;
  }

  const currentTaskIndex = lastCompletedTask + 1;
  if (currentTaskIndex >= studyConditions.length) {
    redirectToSurvey(SURVEY_URL_FINAL, { user_id: userId });
    return null;
  }
  const currentStudyCondition = studyConditions[currentTaskIndex];

  const trackingExtraData = { user: userId, session: currentStudyCondition.session };
  return (
    <Container className="mb-4">
      <Navbar bg="light" className="mb-4">
        <Navbar.Brand>
          <LogoIcon width="30" height="30" className="d-inline-block align-top" />
          {' '}
          typefully
        </Navbar.Brand>
      </Navbar>

      {!consent && <WelcomeScreen onComplete={() => setConsent(true)} username={username} setUsername={setUsername} />}

      {consent && (
        <SessionRecorder trackingExtraData={trackingExtraData} saveFn={(data) => API.post('/log-recording', data)}>
          <LoggingRecorder
            trackingExtraData={{ ...trackingExtraData, task: currentStudyCondition.task.title }}
            saveFn={(data) => API.post('/log', data)}
          >
            <Experiment
              task={currentStudyCondition.task}
              initialCondconf={currentStudyCondition.condconf}
              username={username}
              onComplete={async () => {
                await setLastCompletedTask(currentTaskIndex);
                redirectToSurvey(SURVEY_URL_AFTER_TASK, {
                  task_name: currentStudyCondition.task.title,
                  suggestion_count: currentStudyCondition.condconf.numberOfSuggestions,
                  session_id: currentStudyCondition.session,
                  user_id: userId,
                });
              }}
            />
          </LoggingRecorder>
        </SessionRecorder>
      )}
    </Container>
  );
};

const ErrorMessage = (title: string, body: string) => (
  <Container>
    <Alert variant="danger" className="my-5">
      <Alert.Heading>
        Oh snap!
        {' '}
        {title}
      </Alert.Heading>
      <p>{body}</p>
    </Alert>
  </Container>
);

export default App;

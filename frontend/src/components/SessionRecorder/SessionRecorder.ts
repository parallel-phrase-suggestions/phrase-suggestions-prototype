/* eslint-disable lines-between-class-members */
import React from 'react';
import { record } from 'rrweb';
import { v4 as uuidv4 } from 'uuid';

import { listenerHandler } from 'rrweb/typings/types';
import Log from '../../util/Log';

const API_SEND_INTERVAL = 5000;

type TrackingExtraData = Record<string, string | number | boolean>;

type eventWithTimeAndSession = { recording: string; timestamp: number; data: string } & TrackingExtraData;
type saveFn = (data: eventWithTimeAndSession[]) => Promise<void>;
type SessionRecorderProps = {
  trackingExtraData: TrackingExtraData;
  saveFn: saveFn;
  children?: React.ReactNode;
};

export default class SessionRecorder extends React.Component<SessionRecorderProps, {}> {
  recordingId: string;
  events: eventWithTimeAndSession[];
  stopRec?: listenerHandler;
  timeout?: NodeJS.Timeout;

  constructor(props: SessionRecorderProps) {
    super(props);
    this.recordingId = uuidv4();
    this.events = [];
  }

  componentDidMount() {
    Log.info('Starting session recorder');
    const { trackingExtraData } = this.props;
    const addEvent = (event: Pick<eventWithTimeAndSession, 'data' | 'timestamp'>) =>
      this.events.push({ ...event, ...trackingExtraData, recording: this.recordingId });

    this.stopRec = record({
      emit(event) {
        const { timestamp } = event;
        addEvent({ timestamp, data: JSON.stringify(event) });
      },
    });
    this.sendLogdata();
  }

  componentWillUnmount() {
    if (!this.stopRec) return;
    Log.info('Stopping session recorder');
    this.stopRec();
    this.timeout && clearTimeout(this.timeout);
    this.sendLogdata(false);
  }

  async sendLogdata(loop = true) {
    const { saveFn } = this.props;
    const data = this.events.splice(0);
    try {
      if (data.length) {
        Log.info('Saving %s session recorder data points.', data.length);
        await saveFn(data);
      }
    } catch (err) {
      Log.error('SessionRecorder – error on sending data: %O', err);
      this.events = [...data, ...this.events].sort((a, b) => a.timestamp - b.timestamp);
    } finally {
      if (loop) this.timeout = setTimeout(() => this.sendLogdata(), API_SEND_INTERVAL);
    }
  }

  render() {
    const { children } = this.props;
    return children;
  }
}

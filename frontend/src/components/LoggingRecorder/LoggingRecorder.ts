/* eslint-disable lines-between-class-members */
import React from 'react';
import track from 'react-tracking';

import Log from '../../util/Log';
import { TypefullyWindow, LogPoint } from '../../root-types';

const API_SEND_INTERVAL = 5000;

declare let window: TypefullyWindow;
window.dataLayer = window.dataLayer || [];

type TrackingExtraData = Record<string, string | number | boolean>;
type saveFn = (data: LogPoint[]) => Promise<void>;
type LoggingRecorderProps = {
  trackingExtraData: TrackingExtraData;
  saveFn: saveFn;
  children?: React.ReactNode;
};

@track((props: LoggingRecorderProps) => props.trackingExtraData, {
  dispatch: (data) => {
    const timestamp = Date.now();
    (window.dataLayer = window.dataLayer || []).push({
      ...data,
      timestamp,
      elapsedSinceExperimentStart: window.startTime ? timestamp - window.startTime : null,
    });
  },
})
export default class LoggingRecorder extends React.Component<LoggingRecorderProps, {}> {
  timeout?: NodeJS.Timeout;

  @track({ action: 'session-started' })
  componentDidMount() {
    Log.info('Starting logging recorder');
    window.startTime = Date.now();
    this.sendLogdata();
  }

  componentWillUnmount() {
    Log.info('Stopping logging recorder');
    this.timeout && clearTimeout(this.timeout);
    this.sendLogdata(false);
  }

  async sendLogdata(loop = true) {
    const { saveFn } = this.props;
    const data = window.dataLayer.splice(0);
    try {
      if (data.length) {
        Log.info('Saving %s logging recorder data points.', data.length);
        await saveFn(data);
      }
    } catch (err) {
      Log.error('LoggingRecorder – error on sending data: %O', err);
      window.dataLayer = [...data, ...window.dataLayer];
    } finally {
      if (loop) this.timeout = setTimeout(() => this.sendLogdata(), API_SEND_INTERVAL);
    }
  }

  render() {
    const { children } = this.props;
    return children;
  }
}

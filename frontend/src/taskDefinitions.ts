import { Task, ConditionConfig } from './root-types';

export const tasks: Task[] = [
  {
    title: 'Reference-Google',
    description: `You need to write an email to your former peer Jane, asking her if she would be available 
                  as a reference for an application at Google. She recently got promoted to Manager, 
                  which you should mention.`,
    to: 'Jane.Wilson@online.com',
    subject: 'Could you help me?',
    replacements: {
      '<|PERSON|>': 'Jane',
      '<|DATE|>': 'in the next days',
      '<|ORG|>': 'Google',
      '<|TIME|>': 'later',
      '<|GPE|>': 'the UK',
      '<|endoftext|>': '',
    },
  },
  {
    title: 'Congrats-Anna',
    description: `You are a direct report to Anna, who is on a business trip abroad. Today is her birthday, 
                  so congratulate her via email and add some personal wishes!`,
    to: 'Anna.Wilson@greatcompany.com',
    subject: 'Happy Birthday!',
    replacements: {
      '<|PERSON|>': 'Anna',
      '<|DATE|>': 'today',
      '<|TIME|>': 'later',
      '<|ORG|>': 'the company',
      '<|GPE|>': 'abroad',
      '<|endoftext|>': '',
    },
  },
  {
    title: 'ID-card',
    description: `Your company badge is about to expire, so you need to email your HR team to get a new badge. 
                  Please explain why you can pick it up next week at the earliest and ask about the procedure!`,
    to: 'hr@my-company.com',
    subject: 'Expiring ID card',
    replacements: {
      '<|PERSON|>': 'HR team',
      '<|DATE|>': 'next week',
      '<|TIME|>': 'afternoon',
      '<|ORG|>': 'the company',
      '<|GPE|>': 'London',
      '<|endoftext|>': '',
    },
  },
  {
    title: 'Facebook-Interview',
    description: `You are a recruiter at Facebook and confirm an appointment for an interview with the candidate Jane
                  you just spoke to on the phone: Next Monday at 11:30 a.m.`,
    to: 'jane.smith@hotmail.com',
    subject: 'Interview at Facebook',
    replacements: {
      '<|PERSON|>': 'Jane',
      '<|DATE|>': 'next Monday',
      '<|TIME|>': '11:30 a.m.',
      '<|ORG|>': 'Facebook',
      '<|GPE|>': 'Menlo Park',
      '<|endoftext|>': '',
    },
  },
  {
    title: 'Dinner-Jason',
    description: `You are travelling to Berlin and would like to meet Jason, a friend from high school who now lives
                  there and works for SAP. You will arrive in two weeks - ask him if he is free for dinner.`,
    to: 'jason.miller@gmail.com',
    subject: 'Dinner in two weeks?',
    replacements: {
      '<|PERSON|>': 'Jason',
      '<|DATE|>': 'two weeks',
      '<|TIME|>': 'evening',
      '<|ORG|>': 'SAP',
      '<|GPE|>': 'Berlin',
      '<|endoftext|>': '',
    },
  },
];

export const conditions: ConditionConfig[] = [
  {
    numberOfSuggestions: 0,
    suggestionLength: 4,
    stopAtSentenceEnd: true,
  },
  {
    numberOfSuggestions: 1,
    suggestionLength: 4,
    stopAtSentenceEnd: true,
  },
  {
    numberOfSuggestions: 3,
    suggestionLength: 4,
    stopAtSentenceEnd: true,
  },
  {
    numberOfSuggestions: 6,
    suggestionLength: 4,
    stopAtSentenceEnd: true,
  },
];

export type Condition = {
  task: Task;
  condconf: ConditionConfig;
  session?: string;
};

// Using this latin square:
// https://en.wikipedia.org/wiki/Graeco-Latin_square#/media/File:GrecoLatinSquare-Order4.svg

export const latinSquare: Array<Condition[]> = [
  [
    { task: tasks[0], condconf: conditions[0] },
    { task: tasks[1], condconf: conditions[2] },
    { task: tasks[2], condconf: conditions[3] },
    { task: tasks[3], condconf: conditions[1] },
  ],
  [
    { task: tasks[1], condconf: conditions[1] },
    { task: tasks[0], condconf: conditions[3] },
    { task: tasks[3], condconf: conditions[2] },
    { task: tasks[2], condconf: conditions[0] },
  ],
  [
    { task: tasks[2], condconf: conditions[2] },
    { task: tasks[3], condconf: conditions[0] },
    { task: tasks[0], condconf: conditions[1] },
    { task: tasks[1], condconf: conditions[3] },
  ],
  [
    { task: tasks[3], condconf: conditions[3] },
    { task: tasks[2], condconf: conditions[1] },
    { task: tasks[1], condconf: conditions[0] },
    { task: tasks[0], condconf: conditions[2] },
  ],
];

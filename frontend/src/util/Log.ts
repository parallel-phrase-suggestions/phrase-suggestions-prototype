import debug from 'debug';
import * as _ from 'lodash';

type LogLevel = 'trace' | 'info' | 'warn' | 'error';

const BASE = 'typefully';
const COLORS: Record<LogLevel, string> = {
  trace: 'lightblue',
  info: 'blue',
  warn: 'pink',
  error: 'red',
};

const createDebug = _.memoize((level: LogLevel) => {
  const instance = debug(`${BASE}:${level}`);
  // Set the colour of the message based on the level
  instance.color = COLORS[level];
  return instance;
});

const generateMessage = (level: LogLevel, message: string, ...formatArgs: any[]) =>
  createDebug(level)(message, ...formatArgs);

class Log {
  static trace(message: string, ...formatArgs: any[]) {
    return generateMessage('trace', message, ...formatArgs);
  }

  static info(message: string, ...formatArgs: any[]) {
    return generateMessage('info', message, ...formatArgs);
  }

  static warn(message: string, ...formatArgs: any[]) {
    return generateMessage('warn', message, ...formatArgs);
  }

  static error(message: string, ...formatArgs: any[]) {
    return generateMessage('error', message, ...formatArgs);
  }
}

export default Log;

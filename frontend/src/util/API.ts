import axios from 'axios';

const apiBaseUrl = process.env.REACT_APP_API_BASE_URL || window.location.origin.toString();
if (!apiBaseUrl) {
  throw new Error("API's base URL not set");
}

export const getCancelTokenSource = () => axios.CancelToken.source();
export const isCanceled = (error: any) => axios.isCancel(error);

const client = axios.create({
  baseURL: new URL('/', apiBaseUrl).toString(),
  responseType: 'json'
});

export default client;

export type LogPoint = Record<string, string | number | null | any>;

export interface TypefullyWindow extends Window {
  dataLayer: LogPoint[];
  startTime?: number;
}

export type GenerationApiResponseItem = {
  text: string;
  score: number;
};
export type GenerationApiResponse = GenerationApiResponseItem[];

// Spacy
type Ent = {
  start: number;
  end: number;
  label: string;
};
type Sent = {
  start: number;
  end: number;
};
type Token = {
  dep: string;
  end: number;
  head: number;
  id: number;
  pos: string;
  start: number;
  tag: string;
};
export type SpacyDocument = {
  ents: Ent[];
  sents: Sent[];
  text: string;
  tokens: Token[];
};

export type ConditionConfig = {
  numberOfSuggestions: number;
  suggestionLength: number;
  stopAtSentenceEnd: boolean;
};

export type Task = {
  title: string;
  description: string;
  to: string;
  subject: string;
  replacements: Record<string, string>;
};

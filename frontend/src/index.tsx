import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';

import './index.scss';

if (process.env.NODE_ENV !== 'production') {
  localStorage.setItem('debug', 'typefully:*');
}

ReactDOM.render(<App />, document.getElementById('root'));

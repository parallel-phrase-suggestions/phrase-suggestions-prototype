# build environment
FROM node:12 as build
ENV GENERATE_SOURCEMAP 'false'

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY ./frontend .
RUN yarn --frozen-lockfile
RUN yarn run build


FROM python:3.6

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

ENV FLASK_APP "./app/main.py"
ENV FLASK_ENV "production"

#### Setup flask
WORKDIR /opt/app

RUN pip install --upgrade pip \ 
    && pip install pipenv

# Install dependencies: skip virtualenv; use only lockfile
ADD ./backend/Pip* ./
RUN pipenv install --system --deploy --ignore-pipfile

# Add code
ADD ./backend .
COPY --from=build /app/build ./app/static
RUN ls -laR

EXPOSE 80
CMD ["gunicorn", "-c", "gunicorn_config.py", "wsgi:app"]

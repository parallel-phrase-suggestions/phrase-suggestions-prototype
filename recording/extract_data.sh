#!/bin/sh

# Install tool via
# `npm install elasticdump -g`

rec=$1
if [ -z "$rec" ]
then
   echo "No recording ID given.";
   exit 1
fi

# Backup index data to a file:
elasticdump \
  --input=http://localhost:9200/recording \
  --type=data \
  --searchBody="$(printf '{"query": { "match_phrase": { "recording": "%s" } }, "stored_fields": ["*"], "_source": true }' ${rec})" \
  --output=$ \
  | jq --compact-output '._source.data|fromjson' \
  | jq --slurp > ./recording_data_${rec}.json

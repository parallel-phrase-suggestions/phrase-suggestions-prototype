#!/bin/sh

# Install dependencies: Including dev devpendencies; skip virtualenv; use only lockfile
pipenv install --system --deploy --ignore-pipfile
pipenv clean

exec "$@"

import os

worker_tmp_dir = '/dev/shm'
worker_class = 'gthread'
workers = 2
threads = 4
timeout = 120
keepalive = 2
bind = '0.0.0.0:{}'.format(os.getenv('PORT', '80'))
accesslog = '-'
errorlog = '-'

import os

from elasticsearch import Elasticsearch, RequestsHttpConnection
from requests_aws4auth import AWS4Auth
from elasticsearch.helpers import bulk

INIDICES_DEFINITIONS = {
    'study': {
        "mappings": {
            "properties": {
                "user": {"type": "keyword"},
                "session": {"type": "keyword"},
                "task": {"type": "keyword"},
                "page": {"type": "keyword"},
                "action": {"type": "keyword"},
                "timestamp": {"type": "date"},
                "elapsedSinceExperimentStart": {"type": "long"}
            }
        }
    },
    'recording': {
        "mappings": {
            "properties": {
                "user": {"type": "keyword"},
                "session": {"type": "keyword"},
                "recording": {"type": "keyword"},
                "timestamp": {"type": "date"},
                "data": {"type": "text"}
            }
        }
    }
}

# Loading ES
if os.getenv('ES_AWS_HOST'):
    awsauth = AWS4Auth(os.environ['AWS_ACCESS_KEY_ID'], os.environ['AWS_SECRET_ACCESS_KEY'],
                       os.environ['AWS_DEFAULT_REGION'], 'es')
    es = Elasticsearch(
        hosts=[{'host': os.environ['ES_AWS_HOST'], 'port': 443}],
        http_auth=awsauth,
        use_ssl=True,
        verify_certs=True,
        connection_class=RequestsHttpConnection
    )
else:
    es = Elasticsearch([os.environ['ES_URL']])
print(es.info())

# Set indices
for index, body in INIDICES_DEFINITIONS.items():
    es.indices.create(index=index, body=body, ignore=400)


def write_bulk(index, data):
    if not (index in INIDICES_DEFINITIONS):
        raise Exception("Unknown index %s" % index)

    data = list(data)

    print('Logging: ', data)
    bulk(client=es, index=index, actions=data)


def get_unique_count(index, field):
    # https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-metrics-cardinality-aggregation.html
    aggregation = {
        "size": "0",
        "aggs": {
            "distinct_count": {
                "cardinality": {
                    "field": field,
                    "precision_threshold": 10000
                }
            }
        }
    }
    res = es.search(body=aggregation, index=index)
    return res['aggregations']['distinct_count']

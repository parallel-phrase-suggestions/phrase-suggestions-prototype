import os
import spacy
import sagemaker
from sagemaker.predictor import json_serializer, json_deserializer, RealTimePredictor


# Sagemaker
sagemaker_session = sagemaker.Session()
predictor = RealTimePredictor(os.environ['SAGEMAKER_ENDPOINT_NAME'],
                              sagemaker_session=sagemaker_session,
                              serializer=json_serializer,
                              deserializer=json_deserializer)


# spaCy
nlp = spacy.load("en_core_web_sm")
print('Loading model completed.')


def clamp(minimum, x, maximum):
    return max(minimum, min(x, maximum))


def split_sentence(text, tokens):
    if not tokens:
        return text
    for token in tokens:
        tmp = text.split(token)
        if len(tmp) > 1 and len(tmp[0].strip()) > 0:
            text = tmp[0] + token

    return text


def predict_sentence(text, top_k=1, length=15):
    return predictor.predict({
        'prompt_text': text,
        'length': length,
        'num_return_sequences': top_k,
        'num_beams': top_k*2,
        'early_stopping': True,
        'repetition_penalty': 1.5,
        # int from `tokenizer.bos_token_id` and `tokenizer.eos_token_id`
        'bos_token_id': 50256,
        'eos_token_id': 50256
    })


def replace_tokens(text, tokens):
    for (key, value) in tokens.items():
        text = text.replace(key, value)
        # Workaround
        text = text.replace(key[:-1], value)
    return text


def unique(list_of_dicts, key):
    tmp_dict = {r[key]: r for r in list_of_dicts}
    return list(tmp_dict.values())


def generate_clause(prompt_text, top_k=10, max_length=15, stop_after_sentence=False, replacements={}):
    out = predict_sentence(prompt_text, top_k, max_length)
    if stop_after_sentence:
        out = [split_sentence(text, ['. ', '! ', ': ', '? ']) for text in out]
    if replacements:
        out = [replace_tokens(text, replacements) for text in out]
    out = set(out)
    out = [nlp(text).to_json() for text in out]
    return out

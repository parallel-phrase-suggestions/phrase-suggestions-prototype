#!/usr/local/bin/python3

import os

from flask import Flask, abort, jsonify, request
from flask_cors import CORS, cross_origin
from flask_talisman import Talisman
import distutils.util

from app.es_logging import write_bulk, get_unique_count
from app.text_generation import generate_clause

app = Flask(__name__, static_url_path='', static_folder='static')
Talisman(app,
         content_security_policy={
             'default-src': "* data: blob: ws: wss: 'unsafe-inline' 'unsafe-eval' 'unsafe-dynamic'"
         },
         force_https=bool(distutils.util.strtobool(os.getenv('FORCE_HTTPS', 'True'))))
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/log', methods=['POST'])
@cross_origin()
def write_log():
    data = request.get_json() or abort(400)
    write_bulk('study', data)
    return "", 201


@app.route('/log-recording', methods=['POST'])
@cross_origin()
def write_recording():
    data = request.get_json() or abort(400)
    write_bulk('recording', data)
    return "", 201


@app.route("/generate-clause", methods=['POST'])
@cross_origin()
def get_gen_clause():
    params = request.get_json() or abort(400)
    print(params)
    text = params.pop('text', None) or abort(400)
    result = generate_clause(text, **params)
    print(result)
    return jsonify(result)


@app.route("/participants", methods=['GET'])
@cross_origin()
def get_participants():
    return jsonify(get_unique_count('study', 'user'))


# to serve frontend
@app.route('/')
def root():
    return app.send_static_file('index.html')

# Parallel Phrase Suggestions

This repository holds the code used for the protype described in the paper [“The Impact of Multiple Parallel Phrase Suggestions on Email Input and Composition Behaviour of Native and Non-Native English Writers”](https://arxiv.org/abs/2101.09157). We built a text editor prototype with a neural language model (GPT-2) that allows to compose emails with different numbers of parallel suggestions.



Dependencies:

- All data is logged to an ElasticSearch database.
- The model is deployed using AWS Sagemaker, which allows utilizing GPUs for inference.
  As the code is quite pluggable deployment using other services or infratstructure is feasible.
- The recording of the session is done using [rrweb](https://github.com/rrweb-io/rrweb). To view a recording, retrieve the data points from ElasticSearch using `recording/extract_data.sh` and open the file in `recording/player.html`.



## Configuration

The task definition, the number of parallel suggestions and their length can be adjusted in the following file: `frontend/src/taskDefinitions.ts`

The model itself is published in a seperate repository and is expected to be deployed to an AWS SageMaker endpoint. It is invoked in the file `backend/app/text_generation.py`. 



## Run via Docker

- To run in **dev** mode:

  ```sh
  docker-compose up
  ```

  Frontend is available at `http://localhost:80`. It will automatically refresh once a file is changed in backend or frontend.

- To run in **prod** mode:

  ```sh
  docker-compose -f docker-compose-prod.yml up
  ```

  Frontend is available at `http://localhost:80`.

The containers should update dependencies automatically, but in case you need to recreate the images without cache: 

```sh
# delete container
docker-compose rm [service-name]

# rebuild container
docker-compose build --no-cache [service-name]
```



## Run directly

#### Backend

Requirements: Python 3.7 (e.g. via `pyenv`), `pipenv` installed

```sh
cd backend && pipenv shell

# for setup
pipenv install

# to run
FLASK_ENV=development FLASK_APP=main.py flask run
```



#### Frontend

Requirements: Node.js 12 (e.g. via `nvm`), yarn (usually included)

```sh
cd frontend

yarn start	
```



## Run tests

*Note: Test coverage is limited at this point.* 

**Frontend**: `cd` to frontend dir and run:

```sh
yarn run test # for test
yarn run lint # for linter
```



**Backend**: `cd` to backend dir and run:

```sh
pipenv run python -m pytest ./app # for test
pipenv run flake8 ./app # for linter
```


